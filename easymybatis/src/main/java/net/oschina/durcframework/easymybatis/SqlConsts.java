/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis;

/**
 * SQL关键字常量类
 */
public class SqlConsts {
	public static final String EMPTY = "";
	public static final String BLANK = " ";
	public static final String LIKE = "LIKE";
	public static final String AND = "AND";
	public static final String OR = "OR";
	public static final String EQUAL = "=";
	public static final String IN = "IN";
	public static final String NOT_IN = "NOT IN";
	public static final String INNER_JOIN = "INNER JOIN";
	public static final String LEFT_JOIN = "LEFT JOIN";
	public static final String RIGHT_JOIN = "RIGHT JOIN";
	public static final String ON = "ON";
	public static final String ASC = "ASC";
	public static final String DESC = "DESC";
	public static final String COMMA = ",";
	public static final String DOT = ".";
}

