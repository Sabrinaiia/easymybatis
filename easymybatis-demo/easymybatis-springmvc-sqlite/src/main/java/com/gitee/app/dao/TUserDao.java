package com.gitee.app.dao;

import com.gitee.app.entity.TUser;

import net.oschina.durcframework.easymybatis.dao.CrudDao;

public interface TUserDao extends CrudDao<TUser> {
}